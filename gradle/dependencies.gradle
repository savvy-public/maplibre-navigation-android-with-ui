ext {

    javaVersion = 11

    androidVersions = [
            minSdkVersion    : 15,
            targetSdkVersion : 34,
            compileSdkVersion: 34,
            buildToolsVersion: '30.0.3'
    ]

    version = [
            mapLibreVersion    : '9.5.2',
            mapLibreService    : '5.9.0',
            mapLibreTurf       : '5.9.0',
            mapLibreAnnotations : '1.0.0',
            autoValue          : '1.7.4',
            autoValueParcel    : '0.2.5',
            junit              : '4.13.2',
            supportLibVersion  : '27.1.1',
            constraintLayout   : '1.1.2',
            mockito            : '4.5.1',
            hamcrest           : '2.0.0.0',
            errorprone         : '2.13.1',
            butterknife        : '8.8.1',
            leakCanaryVersion  : '2.9.1',
            timber             : '5.0.1',
            testRunnerVersion  : '1.0.1',
            espressoVersion    : '3.0.2',
            spoonRunner        : '1.6.2',
            commonsIO          : '20030203.000550',
            robolectric        : '4.7.3',
            lifecycle          : '1.1.1',
            picasso            : '2.71828',
            gmsLocation        : '19.0.1'
    ]

    pluginVersion = [
            checkstyle       : '8.2',
            pmd              : '5.8.1',
            jacoco           : '0.8.1',
            errorprone       : '2.0.2',
            coveralls        : '2.12.0',
            spotbugs         : '1.3',
            gradle           : '4.2.0',
            dependencyGraph  : '0.3.0',
            dependencyUpdates: '0.20.0'
    ]

    dependenciesList = [
            // maplibre
            mapLibre               : "org.maplibre.gl:android-sdk:${version.mapLibreVersion}",
            mapLibreServices       : "org.maplibre.gl:android-sdk-services:${version.mapLibreService}",
            mapLibreTurf           : "org.maplibre.gl:android-sdk-turf:${version.mapLibreTurf}",
            mapLibreAnnotations    : "org.maplibre.gl:android-plugin-annotation-v9:${version.mapLibreAnnotations}",

            // AutoValue
            autoValue              : "com.google.auto.value:auto-value:${version.autoValue}",
            autoValuesParcel       : "com.ryanharter.auto.value:auto-value-parcel:${version.autoValueParcel}",
            autoValuesParcelAdapter: "com.ryanharter.auto.value:auto-value-parcel-adapter:${version.autoValueParcel}",

            // butterknife
            butterKnife            : "com.jakewharton:butterknife:${version.butterknife}",
            butterKnifeProcessor   : "com.jakewharton:butterknife-compiler:${version.butterknife}",

            // support
            supportAnnotation      : "com.android.support:support-annotations:${version.supportLibVersion}",
            supportAppcompatV7     : 'androidx.appcompat:appcompat:1.4.1',
            supportV4              : "com.android.support:support-v4:${version.supportLibVersion}",
            supportDesign          : 'com.google.android.material:material:1.0.0',
            supportRecyclerView    : 'androidx.recyclerview:recyclerview:1.2.0',
            supportCardView        : 'androidx.cardview:cardview:1.0.0',
            supportConstraintLayout: 'androidx.constraintlayout:constraintlayout:2.1.3',

            // architecture
            lifecycleExtensions    : 'androidx.lifecycle:lifecycle-extensions:2.0.0',
            lifecycleCompiler      : 'androidx.lifecycle:lifecycle-compiler:2.0.0',

            // square crew
            timber                 : "com.jakewharton.timber:timber:${version.timber}",
            picasso                : "com.squareup.picasso:picasso:${version.picasso}",
            leakCanaryDebug        : "com.squareup.leakcanary:leakcanary-android:${version.leakCanaryVersion}",

            // instrumentation test
            testSpoonRunner        : "com.squareup.spoon:spoon-client:${version.spoonRunner}",
            testRunner             : "com.android.support.test:runner:${version.testRunnerVersion}",
            testRules              : 'androidx.test:rules:1.1.0',
            testEspressoCore       : 'androidx.test.espresso:espresso-core:3.1.0',
            testEspressoIntents    : "com.android.support.test.espresso:espresso-intents:${version.espressoVersion}",

            // unit test
            junit                  : "junit:junit:${version.junit}",
            mockito                : "org.mockito:mockito-core:${version.mockito}",
            hamcrest               : "org.hamcrest:hamcrest-junit:${version.hamcrest}",
            commonsIO              : "commons-io:commons-io:${version.commonsIO}",
            robolectric            : "org.robolectric:robolectric:${version.robolectric}",

            // play services
            gmsLocation            : "com.google.android.gms:play-services-location:${version.gmsLocation}",

            errorprone             : "com.google.errorprone:error_prone_core:${version.errorprone}"

    ]

    pluginDependencies = [
            gradle           : "com.android.tools.build:gradle:${pluginVersion.gradle}",
            checkstyle       : "com.puppycrawl.tools:checkstyle:${pluginVersion.checkstyle}",
            spotbugs         : "gradle.plugin.com.github.spotbugs:gradlePlugin:${pluginVersion.spotbugs}",
            coveralls        : "gradle.plugin.org.kt3k.gradle.plugin:coveralls-gradle-plugin:${pluginVersion.coveralls}",
            errorprone       : "net.ltgt.gradle:gradle-errorprone-plugin:${pluginVersion.errorprone}",
            dependencyGraph  : "com.vanniktech:gradle-dependency-graph-generator-plugin:${pluginVersion.dependencyGraph}",
            dependencyUpdates: "com.github.ben-manes:gradle-versions-plugin:${pluginVersion.dependencyUpdates}"
    ]
}
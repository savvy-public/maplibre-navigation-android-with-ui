This repo contains the navigation SDK that we use in the Savvy Driver app.

This repo is a fork of the [maplibre-navigation-android](https://github.com/maplibre/maplibre-navigation-android) repo which is itself a fork of the Mapbox Android SDK before it introduced closed source elements and lisencing agreements.
The Maplibre repo removed things like Telemetry and the need for a Mapbox app key.

This repo also includes some work from the [flitsmeister-navigation-android](https://github.com/sw-code/flitsmeister-navigation-android) repo which reintroduced some of the UI features from the Mapbox SDK.

This repo combines these 2 projects, updates alot of dependencies to make sure the project can be run on the latest version of Android and fixes a few bugs. We tried to make as few changes as possible from these 2 previous repos incase there are significant updates on Maplibre which we would like to include in our redistribution.

The project is included as a dependency in the Savvy Driver app and is distributed using [Jitpack](https://jitpack.io/#com.gitlab.savvy-public/maplibre-navigation-android-with-ui)

For more details on the project consult the previous 2 mentioned repos.
